package it.fourget.backend.nasa_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class NasaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(NasaBackendApplication.class, args);
    }
}
