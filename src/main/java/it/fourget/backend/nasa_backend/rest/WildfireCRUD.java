package it.fourget.backend.nasa_backend.rest;

import it.fourget.backend.nasa_backend.dto.NASAWildfireDTO;
import it.fourget.backend.nasa_backend.dto.WildfireDTO;
import it.fourget.backend.nasa_backend.entity.Rectangle;
import it.fourget.backend.nasa_backend.service.WildfireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WildfireCRUD {

    @Autowired
    WildfireService wildfireService;


    @RequestMapping(path = "/nasaWildfire", method = RequestMethod.POST)
    public List<NASAWildfireDTO> getAllNasaWildfire(@RequestBody Rectangle rectangle){
        return wildfireService.getAllNASAWildfire(rectangle);
    }

    @RequestMapping(path = "/wildfire", method = RequestMethod.POST)
    public List<WildfireDTO> getAllWildfires (@RequestBody Rectangle rectangle){
        return wildfireService.getAllWildfires(rectangle);
    }
}
