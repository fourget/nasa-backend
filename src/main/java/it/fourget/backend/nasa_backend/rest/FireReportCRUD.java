package it.fourget.backend.nasa_backend.rest;

import it.fourget.backend.nasa_backend.dto.FireReportDTO;
import it.fourget.backend.nasa_backend.service.ReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FireReportCRUD {

    @Autowired
    ReportingService reportingService;

    @RequestMapping(path = "/report/new", method = RequestMethod.POST)
    public boolean submitReport (@RequestBody FireReportDTO fireReport){
        return reportingService.handleReport(fireReport);
    }

}
