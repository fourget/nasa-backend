package it.fourget.backend.nasa_backend.dao;

import it.fourget.backend.nasa_backend.entity.FireReport;
import it.fourget.backend.nasa_backend.entity.Wildfire;
import org.springframework.data.geo.Box;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WildfireDAO extends MongoRepository<Wildfire, String> {

    List<Wildfire> findByFireReportsContaining(FireReport fireReport);
    List<Wildfire> findByCoordinateWithin(Box box);

}
