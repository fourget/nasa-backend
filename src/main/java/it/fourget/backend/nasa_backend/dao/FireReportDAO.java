package it.fourget.backend.nasa_backend.dao;


import it.fourget.backend.nasa_backend.entity.FireReport;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FireReportDAO extends MongoRepository<FireReport, String> {

    public List<FireReport> findByCoordinateNear(Point coordinate);
}
