package it.fourget.backend.nasa_backend.dao;

import it.fourget.backend.nasa_backend.entity.Coordinate;
import it.fourget.backend.nasa_backend.entity.NASAWildfire;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface NASAWildFireDAO extends MongoRepository<NASAWildfire, Point> {

    //public List<NASAWidlfire> findByCoordinateWithin(Circle circle);
    List<NASAWildfire> findByCoordinateNear(Point point, Distance distance);
    List<NASAWildfire> findByCoordinateWithin(Box box);

}
