package it.fourget.backend.nasa_backend.entity;

public enum EnvironmentType {
    GRASS,
    FOREST,
    URBAN,
    LOWDENSITY,
    HIGHDENSITY,
    PEOPLE,
    NOPEOPLE
}
