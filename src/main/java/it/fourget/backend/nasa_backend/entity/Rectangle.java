package it.fourget.backend.nasa_backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.geo.Point;

@Getter
@Setter
@NoArgsConstructor
public class Rectangle {
    private Coordinate upRight, lowLeft;
}
