package it.fourget.backend.nasa_backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

@Getter
@Setter
@NoArgsConstructor
public class FireReport {

    @Id
    private Long timestamp;
    private String userID;
    @GeoSpatialIndexed
    private Point coordinate;
    private Double precision;
    private String picture;
    private Double priority;
    private Double score;
}
