package it.fourget.backend.nasa_backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class NASAWildfire {

    @Id
    String ID;

    @GeoSpatialIndexed()
    private Point coordinate;
    private Date date;

    public NASAWildfire(Point coordinate, Date date) {
        this.coordinate = coordinate;
        this.date = date;
    }
}
