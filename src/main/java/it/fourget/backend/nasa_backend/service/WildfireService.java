package it.fourget.backend.nasa_backend.service;

import it.fourget.backend.nasa_backend.dao.NASAWildFireDAO;
import it.fourget.backend.nasa_backend.dao.WildfireDAO;
import it.fourget.backend.nasa_backend.dto.NASAWildfireDTO;
import it.fourget.backend.nasa_backend.dto.WildfireDTO;
import it.fourget.backend.nasa_backend.entity.Coordinate;
import it.fourget.backend.nasa_backend.entity.NASAWildfire;
import it.fourget.backend.nasa_backend.entity.Rectangle;
import it.fourget.backend.nasa_backend.entity.Wildfire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WildfireService {

    @Autowired
    NASAWildFireDAO nasaWildFireDAO;

    @Autowired
    WildfireDAO wildfireDAO;

    public List<WildfireDTO> getAllWildfires(Rectangle rectangle) {
        Box box = new Box(new Point(rectangle.getLowLeft().getLatitude(), rectangle.getLowLeft().getLongitude()), new Point(rectangle.getUpRight().getLatitude(), rectangle.getUpRight().getLongitude()));
        List<Wildfire> list = wildfireDAO.findByCoordinateWithin(box);
        List<WildfireDTO> newList = new ArrayList<>();
        for (Wildfire wildfire : list) {
            newList.add(new WildfireDTO(new Coordinate(wildfire.getCoordinate().getX(), wildfire.getCoordinate().getY())));
        }
        return newList;
    }

    public List<NASAWildfireDTO> getAllNASAWildfire(Rectangle rectangle) {
        Box box = new Box(new Point(rectangle.getLowLeft().getLatitude(), rectangle.getLowLeft().getLongitude()), new Point(rectangle.getUpRight().getLatitude(), rectangle.getUpRight().getLongitude()));
        List<NASAWildfire> list =  nasaWildFireDAO.findByCoordinateWithin(box);
        List<NASAWildfireDTO> newList = new ArrayList<>();
        for (NASAWildfire wildfire : list) {
            Coordinate coordinate = new Coordinate(wildfire.getCoordinate().getX(), (wildfire.getCoordinate().getY()));
            newList.add(new NASAWildfireDTO(coordinate, wildfire.getDate()));
        }
        return newList;
    }
}
