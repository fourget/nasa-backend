package it.fourget.backend.nasa_backend.service;

import com.mongodb.client.model.geojson.Position;
import it.fourget.backend.nasa_backend.dao.FireReportDAO;
import it.fourget.backend.nasa_backend.dao.NASAWildFireDAO;
import it.fourget.backend.nasa_backend.dao.WildfireDAO;
import it.fourget.backend.nasa_backend.dto.FireReportDTO;
import it.fourget.backend.nasa_backend.entity.*;
import it.fourget.backend.nasa_backend.retroService.PhotoRetroService;
import it.fourget.backend.nasa_backend.util.WebMercator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportingService {

    final static double radius = 0.35 / 2;

    @Autowired
    private FireReportDAO fireReportDAO;

    @Autowired
    private NASAWildFireDAO nasaWildFireDAO;

    @Autowired
    private WildfireDAO wildfireDAO;

    public boolean handleReport(FireReportDTO fireReportDTO) {
        boolean response = true;
        double score = 0.0;
        ModelMapper modelMapper = new ModelMapper();
        FireReport fireReport = new FireReport();
        modelMapper.map(fireReportDTO, fireReport);


        //control nasa database
        if(getNASAInfo(fireReportDTO.getCoordinate())) {
            modelMapper.map(fireReportDTO, fireReport);
            return response;
        }
        fireReport.setPriority(computePriority(fireReportDTO.getEnv()));
        //control photo conformity
        if(fireReportDTO.getPicture() != null) {
            boolean photoResult = false;
            try {
                photoResult = checkPhotoCorrispondence(fireReportDTO.getPicture());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (photoResult) {
                fireReport.setScore(0.3);
            }
        }

        //control other reports matching
        Wildfire wildfire = checkOtherReports(fireReportDTO.getCoordinate(), fireReportDTO.getPrecision(), fireReport.getScore());

        fireReportDAO.save(fireReport);

        if (wildfire != null)
            wildfireDAO.save(wildfire);

        return response;
    }

    private Double computePriority(List<EnvironmentType> envSelected) {
        Double score = 0.0;
        for (EnvironmentType environmentType : envSelected) {
            switch (environmentType) {
                case GRASS:
                    score += 0.25;
                    break;
                case URBAN:
                    score += 0.4;
                    break;
                case FOREST:
                    score += 0.35;
                    break;
                case PEOPLE:
                    score += 0.7;
                    break;
                case NOPEOPLE:
                    score += 0.3;
                    break;
                case LOWDENSITY:
                    score += 0.3;
                    break;
                case HIGHDENSITY:
                    score += 0.7;
                    break;
                default:
                    break;
            }
        }
        return score;
    }

    public boolean checkPhotoCorrispondence(String photo) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.2.148.248:5000")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        PhotoRetroService service = retrofit.create(PhotoRetroService.class);
        return service.checkPhoto(photo).execute().body();
    }

    private boolean getNASAInfo(Coordinate coordinate) {
        //Point center = new Point(coordinate.getLatitude(), coordinate.getLongitude());
        Point center = new Point(coordinate.getLatitude(), coordinate.getLongitude());
        Distance distance = new Distance(radius, Metrics.KILOMETERS);
        //List<NASAWidlfire> list = nasaWildFireDAO.findByCoordinateWithin(new Circle(center, radius));
        List<NASAWildfire> list = nasaWildFireDAO.findByCoordinateNear(center, distance);
        if (list.size() > 1)
            return true;
        return false;
    }


    private Wildfire checkOtherReports(Coordinate coordinate, double precision, double score) {

        Point point = new Point(coordinate.getLatitude(), coordinate.getLongitude());
        List<FireReport> list = fireReportDAO.findByCoordinateNear(point);
        List<FireReport> nearReport = new ArrayList<>();

        double x1, x2, y1, y2, distance;
        boolean flag = false;
        for (FireReport f : list) {
            Point point1 = f.getCoordinate();
            x1 = WebMercator.longitudeToX(point1.getY());
            x2 = WebMercator.longitudeToX(coordinate.getLongitude());
            y1 = WebMercator.latitudeToY(point1.getX());
            y2 = WebMercator.latitudeToY(coordinate.getLatitude());
            distance = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
            if (distance < precision + f.getPrecision()) {
                double priority = f.getPriority();
                if (priority <= 0.95) {
                    f.setPriority(priority + 0.05);
                }
                nearReport.add(f);
                if (f.getPriority() > 0.5) {
                    flag = true;
                }
            } else
                break;
        }

        if(!flag) return null;

        if (nearReport.isEmpty()) return null;

        List<Wildfire> wildfires = new ArrayList<>();
        List<Wildfire> appoList = null;
        List<double[]> coordinates = new ArrayList<>();
        //double wScore = 0.0;
        int count = 0;

        for (FireReport f : nearReport) {
            appoList = wildfireDAO.findByFireReportsContaining(f);
            if (!appoList.isEmpty()) {
                for (Wildfire w : appoList) {
                    wildfires.add(w);
                }
            }
            double[] d = {WebMercator.latitudeToY(f.getCoordinate().getX()), WebMercator.longitudeToX(f.getCoordinate().getY())};
            coordinates.add(d);
            score += f.getScore();
            count++;
        }
        score /= count;

        for (Wildfire w : wildfires) {
            wildfireDAO.delete(w);
        }

        double[] c = {0.0, 0.0};
        count = 0;
        for (double[] d : coordinates) {
            c[0] += d[0];
            c[1] += d[1];
            count++;
        }
        c[0] /= count;
        c[1] /= count;

        Position position1 = new Position(c[0], c[1]);
        Point point1 = new Point(c[0], c[1]);
        return new Wildfire(point1, nearReport, score);
    }


}
