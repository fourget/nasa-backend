package it.fourget.backend.nasa_backend.dto;

import it.fourget.backend.nasa_backend.entity.Coordinate;
import it.fourget.backend.nasa_backend.entity.EnvironmentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class FireReportDTO {

    Long timestamp;
    String userID;
    Coordinate coordinate;
    Double precision;
    String picture;
    List<EnvironmentType> env;


}
