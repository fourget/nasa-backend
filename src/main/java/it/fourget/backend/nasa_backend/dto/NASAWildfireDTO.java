package it.fourget.backend.nasa_backend.dto;

import it.fourget.backend.nasa_backend.entity.Coordinate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class NASAWildfireDTO {
    private Coordinate coordinate;
    private Date date;

}
