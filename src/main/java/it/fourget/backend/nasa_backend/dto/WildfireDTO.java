package it.fourget.backend.nasa_backend.dto;

import it.fourget.backend.nasa_backend.entity.Coordinate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WildfireDTO {

    Coordinate coordinate;
}
