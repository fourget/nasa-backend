package it.fourget.backend.nasa_backend.retroService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.io.File;
import java.util.HashMap;

public interface NASARetroService {
    //@Headers("Authorization: Bearer 5E07894C-D48D-11E8-B0CD-CF089B439298")
    @GET("/api/v2/files/contents/FIRMS/viirs/Global/{file}")
    Call<ResponseBody> getNasaCSVFile(@Path("file") String file, @HeaderMap HashMap<String, String> map/*, @Header("Authorization") String auth*/);
}
