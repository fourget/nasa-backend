package it.fourget.backend.nasa_backend.retroService;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface PhotoRetroService {

    @POST("isFire")
    Call<Boolean> checkPhoto(@Body String photo);
}
