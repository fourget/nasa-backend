package it.fourget.backend.nasa_backend.util;


import com.mongodb.client.model.geojson.Position;
import it.fourget.backend.nasa_backend.dao.NASAWildFireDAO;
import it.fourget.backend.nasa_backend.dao.WildfireDAO;
import it.fourget.backend.nasa_backend.entity.NASAWildfire;
import it.fourget.backend.nasa_backend.entity.Wildfire;
import it.fourget.backend.nasa_backend.retroService.NASARetroService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class FireDataUtil {

    private final String APIKEY = "5E07894C-D48D-11E8-B0CD-CF089B439298";

    @Autowired
    private NASAWildFireDAO dao;

    @Autowired
    private WildfireDAO wildfireDAO;

    //MOCKUP
    @Scheduled(fixedDelay = 10000000)
    public void populateDB(){
        Random random = new Random();
        Double r = 1.0;
        wildfireDAO.deleteAll();
        for (int i = 0; i < 10; i++){
        Wildfire wildfire = new Wildfire(new Point(41.8946366 + r*random.nextDouble(), 12.4946147 + r*random.nextDouble()), null, 0.0);
        wildfireDAO.save(wildfire);
        }
    }

    //get CSV file from NASA
    @Scheduled(fixedDelay = 3600000)
    public void getCSVFile() {

        Calendar calendar = Calendar.getInstance();
        String file = "VIIRS_I_Global_VNP14IMGTDL_NRT_" + calendar.get(Calendar.YEAR) + (calendar.get(Calendar.DAY_OF_YEAR) -1)+ ".txt";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://nrt4.modaps.eosdis.nasa.gov")
                .build();

        NASARetroService service = retrofit.create(NASARetroService.class);

        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("Authorization", "Bearer " + APIKEY);
            String csv = service.getNasaCSVFile(file, map).execute().body().string();
            dao.deleteAll();
            Reader in = new StringReader(csv);
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
            ArrayList<NASAWildfire> list = new ArrayList<>();
            Boolean first = true;
            for (CSVRecord record : records) {
                if (first) {
                    first = false;
                    continue;
                }
                Point coordinate = new Point(Double.valueOf(record.get(0)), Double.valueOf(record.get(1)));
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date d = date.parse(record.get(5) + " " + record.get(6));
                NASAWildfire wildfire = new NASAWildfire(coordinate, d);
                list.add(wildfire);
            }
            dao.insert(list);
            System.out.println("NASA Data inserted correctly");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
