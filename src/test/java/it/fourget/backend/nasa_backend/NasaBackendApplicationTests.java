package it.fourget.backend.nasa_backend;

import it.fourget.backend.nasa_backend.service.ReportingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NasaBackendApplicationTests {

    @Autowired
    ReportingService service;
    @Test
    public void contextLoads() {
        try {
            service.checkPhotoCorrispondence("aaaaaaaaa");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
